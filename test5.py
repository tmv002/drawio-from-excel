from N2G import drawio_diagram
import codecs

def convert_from_windows_1251(filename):
    f = open(filename, 'r')
    ftext = f.read()
    f.close

    t = codecs.open(filename, 'w', 'utf-8')
    t.write(ftext)

diagram = drawio_diagram()
diagram.add_diagram("Page-1")
diagram.add_node(id="R1", label=r'Текст')
diagram.add_diagram("Page-2")
diagram.add_node(id="R2", url="Page-1")
diagram.add_node(id="R3")
diagram.add_link("R2", "R3", label="uplink", data={"speed": "1G", "media": "10G-LR"}, url="http://cmdb.local")
diagram.dump_file(filename="Sample_graph.drawio", folder="./Output")

convert_from_windows_1251("Output\\Sample_graph.drawio")