import codecs

def convert_from_windows_1251(filename):
    f = open(filename, 'r')
    ftext = f.read()
    f.close

    t = codecs.open(filename, 'w', 'utf-8')
    t.write(ftext)

convert_from_windows_1251("Sample_graph.drawio")