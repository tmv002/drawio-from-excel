import openpyxl

def load_ac(exfile):
    acs = {}
    for sheet in exfile._sheets:
        if(sheet['A1'].value == "AC"):
            for row in range(2, sheet.max_row):
                ac_id = sheet['A' + str(row)].value
                ac_name = sheet['B' + str(row)].value
                if ac_name in acs: print(ac_name + " exist for the second times")
                if(ac_id is not None):
                   if(ac_name is None): ac_name = ac_id
                   acs[ac_id] = ac_name
    return acs

def load_integrations(exfile):
    integs = []
    for sheet in exfile._sheets:
        if(sheet['A1'].value == "AC Integration"):
            for row in range(2, sheet.max_row):
                ac1 = sheet['A' + str(row)].value
                line_direction = sheet['B' + str(row)].value
                if(line_direction is None): line_direction = '-'
                ac2 = sheet['C' + str(row)].value
                line_comment = sheet['D' + str(row)].value
                integs.append([ac1, line_direction, ac2, line_comment])
    return integs

def mermaid_acs(acs):
    output = 'flowchart LR\n'
    for ac in acs:
        output += ac + '("' + acs[ac] + '")' '\n'
    return output

def mermaid_integ(integs):
    output = ''
    for integ in integs:
        line = integ[0]
        direction = '---'
        match integ[1]:
            case '>':
                direction = '-->'
            case '=':
                direction = '<-->'
        line += ' ' + direction
        if(integ[3] is None): line += ' '
        else: line += ' |' + integ[3] + '| '
        line += integ[2]
        output += line + ';\n'
    return output


file = openpyxl.load_workbook("AC.xlsx")
acs = load_ac(file)
integs = load_integrations(file)
output = mermaid_acs(acs)
output += mermaid_integ(integs)
print(output)