from N2G import drawio_diagram
import openpyxl
import codecs
from random import randint

def convert_from_windows_1251(filename):
    f = open(filename, 'r')
    ftext = f.read()
    f.close

    t = codecs.open(filename, 'w', 'utf-8')
    t.write(ftext)

def load_ac(exfile):
    acs = {}
    for sheet in exfile._sheets:
        if(sheet['A1'].value == "AC"):
            for row in range(2, sheet.max_row):
                ac_id = sheet['A' + str(row)].value
                ac_name = sheet['B' + str(row)].value
                if ac_name in acs: print(ac_name + " exist for the second times")
                if(ac_id is not None):
                   if(ac_name is None): ac_name = ac_id
                   acs[ac_id] = ac_name
    return acs

def load_integrations(exfile):
    integs = []
    for sheet in exfile._sheets:
        if(sheet['A1'].value == "AC Integration"):
            for row in range(2, sheet.max_row):
                ac1 = sheet['A' + str(row)].value
                line_direction = sheet['B' + str(row)].value
                if(line_direction is None): line_direction = '-'
                ac2 = sheet['C' + str(row)].value
                line_comment = sheet['D' + str(row)].value
                integs.append([ac1, line_direction, ac2, line_comment])
    return integs

def get_nodes_from_ac(acs):
    nodes = []
    for ac in acs:
        nodes.append({'id': ac, 'label': acs[ac].encode().decode()})
    return nodes

def get_links_from_integs(integs):
    colors = []
    for i in range(50):
        colors.append('#%06X' % randint(0, 0xFFFFFF))
    i = 0
    name_ac = ''
    links = []
    for integ in integs:
        if(name_ac != integ[0]):
            name_ac = integ[0]
            i += 1
            if(i > 50): i = 0
        if(integ[3] is None): integ[3] = ''
        print(str(i) + ' ' + colors[i])
        links.append({'source': integ[0], 'style': 'endArrow=none;edgeStyle=orthogonalEdgeStyle;fillColor=#f8cecc;strokeColor=' + colors[i] + ';strokeWidth=1;curved=1;', 'label': integ[3], 'target': integ[2]})
    return links


file = openpyxl.load_workbook("AC.xlsx")
acs = load_ac(file)
integs = load_integrations(file)
nodes = get_nodes_from_ac(acs)
links = get_links_from_integs(integs)
graph = {'nodes': nodes, 'links': links}
print(graph)
diagram = drawio_diagram()
diagram.from_dict(graph)
diagram.layout(algo="circle")
diagram.dump_file(filename="Sample_graph.drawio", folder="./Output/")

convert_from_windows_1251("Output\\Sample_graph.drawio")